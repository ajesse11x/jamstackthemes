---
title: "Milo Bootstrap"
github: https://github.com/sharadcodes/jekyll-theme-milo
demo: https://sharadcodes.github.io/jekyll-theme-milo/
author: Sharad Raj SIngh Maurya
ssg:
  - Jekyll
cms:
  - No Cms
date: 2018-12-22T13:44:48Z
github_branch: master
---
